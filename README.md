# lautomatico

Lautomatico e' un servizio web che permette di interagire con la bobina di radio blackout.
Usa principalmente l'interfaccia telnet di liquisoap (quando `http.register` di liquidsoap sara' stabile
useremo quella).

Per supportare lautomatico liquidsoap deve quindi avere una porta telnet aperta:
```bash
# create a socket to send commands to this instance of liquidsoap
set("control", 12345)
set('server.telnet', true)
```

e una coda prioritaria chiamata `lautomatico`:

```bash
lautomatico = request.queue(id="lautomatico")
playlist = playlist(reload=1, reload_mode="watch", "/home/les/Music/")
stream = fallback(track_sensitive=false, [lautomatico, playlist])
output.alsa(mksafe(stream))
```

## Requirements
- ffprobe
- liquidsoap
- at
- nodejs

## Authentication


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
