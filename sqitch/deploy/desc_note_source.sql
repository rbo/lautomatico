-- Deploy lautomatico:desc_note_source to sqlite

BEGIN;

alter table queue add column description TEXT;
alter table queue add column source TEXT;
alter table queue add column note TEXT;

COMMIT;
