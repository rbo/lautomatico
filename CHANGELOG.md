# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://0xacab.org/rbo/lautomatico/compare/v0.1.1...v1.1.0) (2024-03-02)


### Features

* add description, source, internal note, valid_due to blackholes ([924518d](https://0xacab.org/rbo/lautomatico/commit/924518d8685d1981286081dc84bde2ddf1401ca2))
* add last play in programmate ([ea0145b](https://0xacab.org/rbo/lautomatico/commit/ea0145baed6c7d3b495e1663c0a25cc3504bd888))
* add v18/20 nodejs support ([3a0e276](https://0xacab.org/rbo/lautomatico/commit/3a0e2764cbeb12ec4562912b44eb881b94bc7f01))
* associate audio_id to queue and get past timestamp ([e301be4](https://0xacab.org/rbo/lautomatico/commit/e301be4329e0281bc15a1bce0ac84061ed674f93))
* introduce .env ([5769a8d](https://0xacab.org/rbo/lautomatico/commit/5769a8da6fcedebec2ff16a0c67c4966a496f342))
* introduce sqitch to manage db migrations ([e037544](https://0xacab.org/rbo/lautomatico/commit/e037544de458728c7057c5a5d50aed09b95a2477))
* last play in blackholes table, description, note, source, valid_due, lot of kaos ([57202cf](https://0xacab.org/rbo/lautomatico/commit/57202cf5a540cdbe8615830686a0ff88dab84297))
* tag is searchable ([a9f7a73](https://0xacab.org/rbo/lautomatico/commit/a9f7a7373e201b6e394502db9aa678762745e479))


### Bug Fixes

* add BASE_URL env to configuration ([859fcea](https://0xacab.org/rbo/lautomatico/commit/859fceada45588d4d93eed82ebabcb526a3214be))
* allow audio without tags ([53bf7c6](https://0xacab.org/rbo/lautomatico/commit/53bf7c6415b6d0d13f83e6eb2683914765a59157))
* associate user to queue ([f7cecb6](https://0xacab.org/rbo/lautomatico/commit/f7cecb6dc3f4488d2334a4a0ca02d3f8569c1a4e))
* get env via /env ([28a468f](https://0xacab.org/rbo/lautomatico/commit/28a468f7214d6c42e4e365d8373d70abc46a3078))
* improve DB connection error handling ([74b571f](https://0xacab.org/rbo/lautomatico/commit/74b571f0b0fd81545a19f4cf74bc2605712194c3))
* minor with timers cleaning ([fc040ae](https://0xacab.org/rbo/lautomatico/commit/fc040ae895b5ff3c1fbfb2e7915d1025fec395e9))
* queue with user and > now ([a5eb576](https://0xacab.org/rbo/lautomatico/commit/a5eb576e87dadd4d72baf47d48f3589c62835fa1))
* select items from queue > now ([91fcf61](https://0xacab.org/rbo/lautomatico/commit/91fcf61ab5d975b101309f97410d1433ed871793))
* use required validators to check arrays too ([d0809e7](https://0xacab.org/rbo/lautomatico/commit/d0809e744d72f28427a0807d4fffad5b842057b4))
