
export function runAPI(path, options) {
    // const baseurl = '.' // 'http://localhost:3000' //'http://lautomatico.loc/api'
    // const baseurl = 'http://localhost:3000' //'http://lautomatico.loc/api'
    return fetch(window.lautomatica.BASE_URL + path, options)
        .then(ret => {
            if (ret.status >= 400) {
                throw new Error(`Bad response from server: ${ret.status} ${ret.statusText}`);
            }
            try {
                return ret.json()
            } catch (e) {
                return ''
            }
        })
}
