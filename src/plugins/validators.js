
const validators = {
  required: v => {
    return Array.isArray(v) ? v.length >= 1 : !!v || `Questo devi proprio metterlo`
  },
}


import Vue from 'vue'
const Validator = {
    install (Vue) {
        Vue.prototype.$validators = validators
    }
}

Vue.use(Validator)

export default Validator