import Vue from 'vue'
import Vuetify, { colors } from 'vuetify/lib'
import it from 'vuetify/es5/locale/it'

Vue.use(Vuetify)

export default new Vuetify({
  treeShake: true,
  defaultAssets: false,
  theme: {
    dark: true,
    themes: {
      dark: {
        primary: colors.deepOrange.accent3
      }
    },
    lang: {
      locales: { it },
      current: 'it'
    }
  }
});
