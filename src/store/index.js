import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tokens: {}
  },
  mutations: {
    save_tokens (state, tokens) {
      state.tokens = tokens
    }
  },
  actions: {
    save_tokens ({ commit }, tokens) {
      commit('save_tokens', tokens)
    }
  }
})
