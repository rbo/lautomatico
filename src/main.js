import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import validators from './plugins/validators'
// import permission from './plugins/permission'
import store from './store'
import '@mdi/font/css/materialdesignicons.min.css'

Vue.config.productionTip = false
new Vue({
  router,
  vuetify,
  validators,
  // permission,
  store,
  render: h => h(App)
}).$mount('#app')
