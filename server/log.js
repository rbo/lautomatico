const { createLogger, format, transports } = require('winston')


const con = new transports.Console()

const logger = createLogger({
    level: 'debug',
    format: format.combine( format.errors({stack:true}), format.timestamp(), format.colorize(), format.splat(), format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)),
    transports: [new transports.File({ filename: "file.log" }), con],
    exceptionHandlers: [new transports.File({ filename: "exceptions.log" }), con],
    rejectionHandlers: [new transports.File({ filename: "rejections.log" }), con],
})

module.exports = logger