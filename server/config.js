module.exports = {
    PORT: process.env.PORT || 3000,
    BASE_URL: process.env.BASE_URL || './',
    WORKING_PATH: process.env.WORKING_PATH || './',
    BLACKHOLES_PATH: process.env.BLACKHOLES_PATH || './',
    DB_PATH: process.env.DB_PATH || './lautomatico.db'
}