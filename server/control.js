const net = require('net')

const control = {
    send: command => {
        try {
            const sock = net.connect(1234, '127.0.0.1')
            sock.write(command + "\r\n")
            sock.end()
        } catch (e) {
            console.error(e)
        }
    }
}

module.exports = control