// const Metadata = require('@enviro/metadata')
import db from './db.js'
import Metadata from "@enviro/metadata";

// db.add({ title: 'Prova', tags: JSON.stringify(['test', 'test2']), length: 100, path: 'cioa' })

//requiring path and fs modules
import path from 'path'
import fs from 'fs'
//joining path of directory 
const directoryPath = path.join('..', 'bh', 'BLACK HOLES', 'DEFINITIVI');
//passing directoryPath and callback function


async function importDir (currentPath, tags) {
    const files = fs.readdirSync(currentPath)
    for(const file of files) {
        const fullPath = path.join(currentPath, file)
        const stat = fs.lstatSync(fullPath)
        if (stat.isDirectory()) {
            await importDir(fullPath, [...tags, file])
        } else {
            console.error(file)
            const meta = await Metadata.get(fullPath, { path: "/usr/bin/exiftool" })
            let [h, m, s] = meta[0].Duration.replace(' (approx)','').split(':')
            s = Number(s) + Number(m)*60 + Number(h)*60*60
            const audio = {
                title: file.replace('.mp3', ''),
                tags: JSON.stringify(tags),
                path: fullPath.replace(directoryPath, ''),
                length: s
            }
            // console.error(audio)
            db.add_audio(audio)
        }
    }
}

importDir(directoryPath,[]).catch(e => {
    console.error(e)
})