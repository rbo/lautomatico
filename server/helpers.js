const fs = require('fs')
const path = require('path')
const dayjs = require('dayjs')
require('dayjs/locale/it')
const request = require('request')
const logger = require('./log')
const db = require('./db')
const config = require('./config')
const timers = require('./timers')
const { getAudioDurationInSeconds } = require('get-audio-duration')

//   if (!url || !date || !time || !trasmissione) {
//     return res.status(400).send('Manca qualche parametro (url, date, time, trasmissione)')
//   }
//   const details = { url, date, time, trasmissione }
//   //const fromRBO = RegExp(/^https?:\/\/radioblackout.org/)
//   //if (!fromRBO.test(url)) { return res.status(400).send('L\'url non è sul sito della radio') }

function fill_queue_at_start () {
  const items = db.queue()
  items.forEach(timers.add)
}

fill_queue_at_start()

// download url locally
const download = (details) => {
  const { url, date, time, title } = details
  logger.info("Download di %s", url)
  return new Promise( (res, rej) => {
    const sendReq = request.get(encodeURI(url))
    
    // verify response code
    sendReq.on('response', (response) => {
      if (response.statusCode !== 200) {
        return rej('Response status was ' + response.statusCode)
      }
      
      const type = response.headers['content-type']
      const length = response.headers['content-length']
      
      if (!length || length < 10000) {
        rej(`Il file è troppo piccolo: ${length}`)
      }
      
      if (!['audio/mpeg', 'audio/ogg', 'application/ogg'].includes(type)) {
        rej(`Il file non ha un formato supportato: ${type}`)
      }
      
      details.filename = `${title}_${date}_${time}.${type === 'audio/mpeg' ? 'mp3' : 'ogg'}`
      details.filename = path.join(config.WORKING_PATH, details.filename)
      let file = fs.createWriteStream(details.filename)
      sendReq.pipe(file)
      
      // close() is async, call cb after close completes
      file.on('finish', () => file.close(() => res(details.filename)))
      file.on('error', (err) => { // Handle errors
        logger.error(err)
        fs.unlink(details.filename) // Delete the file async. (But we don't check the result perche' sticazzi)
        return rej(err.message)
      })
    })
    
    // check for request errors
    sendReq.on('error', (err) => {
      fs.unlink(details.filename)
      return rej(err.message)
    })
    
  })
}

const helpers = {
  
  fill_user: function (req, res, next) {
    const authorization = req.headers['authorization']
    if (authorization) {
      const [ user ] = Buffer.from(authorization.replace('Basic ', ''), 'base64').toString('ascii').split(':')
      req.user = user
      // logger.debug('User %s', user)
    } else {
      req.user = 'NO USER'
      //   logger.debug('No user!')
    }
    next()
  },
  
  queue: function (req, res) {
    res.json(db.queue())
  },
  
  remove_enqueue: async function (req, res) {
    const id = parseInt(req.params.id)
    const item = db.get_queue(id)
    if (!item) {
      logger.error("Item not found in queue: %s", id)
      return res.status(404).json({message: 'Item not found'})
    }
    logger.error('[%s] Removing %s', req.user, item)
    
    // se il file era from_file o from_url devo eliminarlo
    
    // remove at 
    timers.remove(id)
    db.remove_enqueue(id)

    res.json('OK')
  },
  
  enqueue: async function (req, res) {
    
    const body = req.body
    const type = body.type
    
    // validate type
    if (!['blackholes', 'from_file', 'from_url'].includes(type)) {
      logger.error("[%s] type not blackholes, from_file or from_url: %s", req.user, type)
      return res.status(400).json({message: 'type not blackholes, from_file or from_url'})
    }
    
    // per ognuno devo tirare fuori
    // un path del file
    // il momento in cui deve partire
    // la lunghezza
    // controllo che in coda non ci sia nulla che overllappi
    // scrivo il file per at, lancio at e mi prendo l'id!
    
    const item = {
      user: req.user,
      type: body.type,
      title: body.title,
      date: body.date,
      time: body.time,
      start_at: dayjs(`${body.date} ${body.time}`).valueOf()
    }
    
    if (type === 'blackholes') {
      const audio = db.get_audio(body.audio_id)
      if (!audio) {
        return res.status(400).json({message: 'Audio not found'})
      }
      item.path = path.join(config.BLACKHOLES_PATH, audio.path)
      item.length = audio.length
      item.title = audio.title
      item.audio_id = audio.id
      item.length = audio.length
      // logger.info('ENQUEUE BLACKHOLES: %s %s %s %s', audio.title, audio.path, body.date, body.time)
      
      // const script = writeScript({ trasmissione: audio.title, filename: audio.path, date: body.date, time: body.time })
      // const at_id = await enqueueScript({ script, date: body.date, time: body.time })
      // db.enqueue({ title: audio.title, at_id, audio_id: audio.id })
      // return res.json(at_id)
    } else if (type === 'from_file') {
      if (!req.file) {
        return res.status(400).send('No file!')
      }
      
      item.path = req.file.path
      item.length = parseInt(await getAudioDurationInSeconds(item.path))
      // item
      
      // TODO: calculate audio length?
      
      // const script = writeScript({ trasmissione: body.trasmissione, filename: req.file.path, date: body.date, time: body.time })
      // const at_id = await enqueueScript({ script, date: body.date, time: body.time })
      // db.enqueue({ title: body.trasmissione, at_id })
      // logger.info('[%s] ENQUEUE FROM PC: %s %s %s %s %s', req.user, item.title, item.path, body.date, body.time, item.length)
    } else if (type === 'from_url'){
      if (!body.url) {
        return res.status(400).send('No URL!')
      }
      item.path = await download(body)
      item.length = parseInt(await getAudioDurationInSeconds(item.path))
      
    }
    
    try {
      const ret = db.enqueue(item)
      logger.info('[%s] Enqueue %s [%s]: %s, queue_id: %s', req.user, type, item.title, item.start_at, ret.lastInsertRowid)
      timers.add(item, ret.lastInsertRowid)
    } catch (e) {
        logger.error(e)
        return res.status(500, String(e))
    }
    
    return res.json('OK')
  },

  update_audio: async function(req, res) {
    const id = parseInt(req.params.id)
    const item = db.get_audio(id)
    if (!item) {
      logger.error("Audio item not found: %s", item.id)
      return res.status(400).json({message: 'Audio not found'})
    }
  
    const audio = {
      title: req.body.title,
      tags: req.body.tags,
      description: req.body.description,
      note: req.body.note,
      valid_due: req.body.valid_due,
      source: req.body.source
    }
          
    logger.info('[%s] Update audio %s (%s, %s)', req.user, item, req.body.title, req.body.tags, req.body.valid_due)
    db.update_audio(id, audio)
    res.json('OK')
  },
  
  remove_audio: async function (req, res) {
    const id = parseInt(req.params.id)
    const item = db.get_audio(id)
    logger.info('[%s] Removing audio %s', req.user, item)
    if (!item) {
      logger.error("Audio item not found: %s", item.id)
      return res.status(400).json({message: 'Audio not found'})
    }
    
    // remove file from fs
    
    db.remove_audio(id)
    
    res.json('OK')
    
  },
  
  add_audio: async function (req, res) {
    const body = req.body
    const file = req.file
    
    // TODO: should check the file
    if (!file) {
      return res.sendStatus(400)
    }
    
    const length = parseInt(await getAudioDurationInSeconds(file.path))
    const audio = {
      title: body.title,
      tags: body.tags,
      description: body.description,
      note: body.note,
      valid_due: body.valid_due,
      source: body.source,
      length,
      path: path.basename(file.path)
    }
    
    logger.info('[%s] ADD BLACKHOLES %s %s %s', req.user, audio.title, audio.tags, audio.path)
    
    return res.json(db.add_audio(audio))
    
  },

  _stream (req, res, path) {
    let stat
    try {
      stat = fs.statSync(path)
      if (!stat) {
        return res.sendStatus(400)
      }
    } catch (e) {
      return res.sendStatus(400)
    }
    const range = req.headers.range
    let readStream

    // if there is no request about range
    if (range !== undefined) {
      // remove 'bytes=' and split the string by '-'
      const parts = range.replace(/bytes=/, "").split("-");
      
      const partial_start = parts[0];
      const partial_end = parts[1];
      
      if ((isNaN(partial_start) && partial_start.length > 1) || (isNaN(partial_end) && partial_end.length > 1)) {
        return res.sendStatus(500);         
      }
      // convert string to integer (start)
      const start = parseInt(partial_start, 10);    // convert string to integer (end)
      // if partial_end doesn't exist, end equals whole file size - 1
      const end = partial_end ? parseInt(partial_end, 10) : stat.size - 1;    // content length
      const content_length = (end - start) + 1;
      
      res.status(206).header({
        'Content-Type': 'audio/mpeg',
        'Content-Length': content_length,
        'Content-Range': "bytes " + start + "-" + end + "/" + stat.size
      });
      
      // Read the stream of starting & ending part
      readStream = fs.createReadStream(path, {start: start, end: end});
    } else {
      res.header({
        'Content-Type': 'audio/mpeg',
        'Content-Length': stat.size
      });
      readStream = fs.createReadStream(path);
    }
    readStream.pipe(res);
  },
  
  stream_audio (req, res) {
    // could be from queue or from blackholes
    const id = parseInt(req.params.id)
    const audio = db.get_audio(id)

    if (!audio) {
      return res.sendStatus(404)
    }

    const audiopath = path.join(config.BLACKHOLES_PATH, audio.path)
    return helpers._stream(req, res, audiopath)

  },

  stream_queue (req, res) {
    const id = parseInt(req.params.id)
    const item = db.get_queue(id)
    if (!item) {
      return res.sendStatus(404)
    }

    return helpers._stream(req, res, item.path)

  }
}


module.exports = helpers
