// const Metadata = require('@enviro/metadata')
import db from './db'
import Metadata from "@enviro/metadata";

// db.add({ title: 'Prova', tags: JSON.stringify(['test', 'test2']), length: 100, path: 'cioa' })

//requiring path and fs modules
const path = require('path');
const fs = require('fs');
//joining path of directory 
const directoryPath = path.join(__dirname, '..', 'bh', 'BLACK HOLES', 'DEFINITIVI');
//passing directoryPath and callback function


async function importDir (currentPath, tags) {
    const files = fs.readdirSync(currentPath)
    for(const file of files) {
        const fullPath = path.join(currentPath, file)
        const stat = fs.lstatSync(fullPath)
        if (stat.isDirectory()) {
            await importDir(fullPath, [...tags, file])
        } else {
            console.error(file)
            const audio = {
                title: file.replace('.mp3', ''),
                tags: JSON.stringify(tags),
                path: fullPath.replace(directoryPath, '')
            }
            const meta = await Metadata(fullPath)
            console.error(Metadata)
            // db.add(audio)
        }
    }
}

importDir(directoryPath,[])