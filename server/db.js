// DB schema
// audio
//   uuidv4, titolo, descrizione, tags, indice, gruppo, data inserimento, valido fino a, durata
/**

CREATE TABLE audio (
  id INTEGER PRIMARY KEY,
  title TEXT NOT NULL,
  tags JSON,
  idx INTEGER DEFAULT 0,
  description TEXT,
  source TEXT,
  note TEXT,
  insert_at integer(4) not null default (strftime('%s','now')),
  valid_due  integer(4) null,
  length INTEGER(4),
  path TEXT);

CREATE VIEW AudioWithTags AS 
    SELECT audio.id, json_each.value AS tag FROM audio
    JOIN json_each(audio.tags) ON 1;

CREATE INDEX title on audio(title);

CREATE TABLE queue (
  id INTEGER PRIMARY KEY,
  title TEXT,
  audio_id INTEGER, -- in case type is blackhole
  at_id INTEGER,
  insert_at integer(4) DEFAULT (strftime('%s','now')),
  start_at  integer(4),
  type TEXT, -- blackhole, from_file, from_url, staffetta?
  path TEXT,
  user TEXT,
  length integer(4) -- seconds
);

*/

const config = require('./config')
// const logger = require('./log')

// cerca tra gli audio
const options = {}
const sqlite3 = require('better-sqlite3')

let connection

try {
    connection = sqlite3(config.DB_PATH, options)
    connection.pragma('journal_mode = WAL')
} catch (e) {
    console.error(String(e))
    console.error(config.DB_PATH)
    process.exit(-1)
}

const db = {

    connection,

    add_audio (audio) {
        const stmt = connection.prepare('INSERT INTO audio (title, tags, description, note, source, length, path, valid_due) VALUES (?, ?, ?, ?, ?, ?, ?, ?)')
        return stmt.run(audio.title, audio?.tags ?? [], audio?.description || null, audio?.note || null, audio?.source || null, audio.length, audio.path, audio?.valid_due)
    },

    get_audio (id) {
        const stmt = connection.prepare('SELECT * FROM audio WHERE audio.id=@id')
        return stmt.get({ id })
    },

    remove_audio (id) {
        const stmt = connection.prepare('DELETE FROM audio where id = ?')
        return stmt.run(id)        
    },

    update_audio (id, audio) {
        const stmt = connection.prepare('UPDATE audio SET title=?, tags=?, description=?, note=?, source=?, valid_due=? WHERE id=?')
        return stmt.run(audio.title,
            JSON.stringify(audio?.tags ?? []),
            audio?.description ?? null,
            audio?.note ?? null,
            audio?.source ?? null,
            audio?.valid_due ?? null,
            id)
    },

    // search for blackholes
    search (req, res) {
        const search = req.query.query
        const limit = req.query.limit | -1

        let stmt
        let ret
        if (search) {
            stmt = connection.prepare(`SELECT audio.id, audio.title, tags, valid_due, audio.length, audio.description,
                audio.source, audio.note, group_concat(DISTINCT queue.start_at) last_play FROM audio
                LEFT JOIN queue ON queue.audio_id=audio.id AND queue.audio_id IS NOT NULL
                LEFT JOIN AudioWithTags at on at.id=audio.id WHERE audio.title like @search OR at.tag like @search  GROUP BY audio.id LIMIT @limit`)
            ret = stmt.all({ search: '%' + search + '%', limit }) //, search, search)
        } else {
            stmt = connection.prepare(`SELECT audio.id, audio.title, tags, valid_due, audio.length, audio.description,
                audio.source, audio.note, group_concat(DISTINCT queue.start_at) last_play FROM audio
                LEFT JOIN queue ON queue.audio_id=audio.id AND queue.audio_id IS NOT NULL GROUP BY audio.id LIMIT @limit`)
            ret = stmt.all({ limit })
        }

        for(const audio of ret) {
            audio.tags = audio.tags && JSON.parse(audio.tags)
        }
        return res.json(ret)
    },


    search_tags (req, res) {
        const search = req.query.query
        const stmt = connection.prepare('SELECT COUNT(*) n, tag FROM AudioWithTags WHERE tag like @search GROUP BY tag ORDER BY n DESC')
        const ret = stmt.all({ search: '%' + search + '%' })
        return res.json(ret)
    },

    /** QUEUE MANAGEMENT */
    // retrieve the queue
    queue() {
        const stmt = connection.prepare('SELECT * from queue where start_at/1000 > 1*strftime(@s, @now) order by start_at')
        return stmt.all({ s: '%s', now: 'now'})
    },

    enqueue ({ title, start_at, audio_id, length, path, user }) {
        const stmt = connection.prepare('INSERT INTO queue (title, start_at, audio_id, path, length, user) VALUES(?,?,?,?,?,?)')
        return stmt.run(title, start_at, audio_id, path, length, user)
    },

    remove_enqueue (id) {
        const stmt = connection.prepare('DELETE FROM queue where id = ?')
        return stmt.run(id)
    },

    get_queue (id) {
        const stmt = connection.prepare('SELECT * from queue where id = ?')
        return stmt.get(id)
    }
    // mostrami la coda
}

module.exports = db
