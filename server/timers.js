// const logger = require('./log')
const dayjs = require('dayjs')
const fs = require('fs')
const control = require("./control")
const db = require('./db')
const logger = require('./log')
const setTimeout = require('safe-timers').setTimeout

const timers = {
    timers: [],

    // adds a timer
    add: (details, queue_id) => {
        const timer = setTimeout(timers.execute,  dayjs(details.start_at).diff(), details, queue_id || details.id )
        timers.timers.push({ ...details, timer, queue_id })
    },

    execute: (details, queue_id) => {
        logger.info(`Start ${JSON.stringify(details)} queue_id: ${queue_id}`)
        timers.timers = timers.timers.filter(t => t.queue_id !== details.queue_id)
        try {
            control.send(`lautomatico.push ${details.path}`)
        } catch (e) {
            logger.error(String(e))
        }
        setTimeout(() => {
            try {
                // should remove the file only if from_file || from_url
                if (['from_file', 'from_url'].includes(details.type)) {
                    logger.info(`Remove ${details.path}`)
                    fs.unlinkSync(details.path)
                }
            } catch (e) {
                console.error(e)
                logger.warning(e)
            }
        }, (20*details.length)*1000)
    },

    remove: queue_id => {

        // clear timer
        const timer = timers.timers.find(t => t.queue_id === queue_id)
        if (timer) {
            timer.timer.clear()
            // remove it from timers
            timers.timers = timers.timers.filter(t => t.queue_id !== queue_id)
        }
        // removes it from db
        db.remove_enqueue(queue_id)
    }
}

module.exports = timers