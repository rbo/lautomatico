const express = require('express')
const cors = require('cors')
require('dotenv').config()
const helpers = require('./helpers')
const config = require('./config')

console.error('INIT LAUTOMATICA')
console.error("CONFIGURATION: %s", config)

const db = require('./db')
const app = express()

// serve static website! we should not use this but nginx try_files...
app.use(express.static('./dist/'))

app.use(express.json())
app.use(cors())

// fill user data
app.use(helpers.fill_user)

app.get('/env', (req, res) => res.send( `window.lautomatica = { BASE_URL: "${config.BASE_URL}" }`))

// search for local audio (blackholes)
app.get('/audio/search', db.search)
app.delete('/audio/:id', helpers.remove_audio)
app.put('/audio/:id', helpers.update_audio)
app.get('/tag', db.search_tags)

// insert a new black hole
const bk_upload  = require('multer')({ dest: config.BLACKHOLES_PATH })
app.post('/audio', bk_upload.single('file'), helpers.add_audio)


/** AUDIO */
// get the current queue
app.get('/queue', helpers.queue)

// remove a queue's entry
app.delete('/queue/:id', helpers.remove_enqueue)

// add an entry to the queue
const file_upload  = require('multer')({ dest: config.WORKING_PATH })
app.post('/queue', file_upload.single('file'), helpers.enqueue)

app.use('/listen/audio/:id', helpers.stream_audio)
app.use('/listen/queue/:id', helpers.stream_queue)


//** SPOTTINI */

// list spottini

// remove spottino

// add spottino



app.listen(config.PORT, () => console.log(`LAUTOMATICO listening at http://localhost:${config.PORT}`))
